# Voting App Monorepo Design and Detailed Configuration.


## Voting app:
A simple distributed application running across multiple Docker containers.
The text clearly describes the project as a small and simple microservice application intended for learning GitLab CI/CD and deployment on a Docker infrastructure server.
![voting app design](images/voting-app-design.png)

## Monorepo and Multi-repo:
![Monorepo vs Multi-repo](images/monorepo-vs-multi-repo.png)

**What is a monorepo?** A monorepo is not a monolith. It’s a software development strategy in which a company's entire codebase is stored in a single repository.

**What is a multi-repo?** A multi-repo is a software development approach where different projects or components of a larger application are stored and managed as separate repositories. Each project or component has its own version control repository, allowing teams to work on them independently. This approach contrasts with a monorepo, where all code is stored in a single repository. Multi-repo setups are often used to maintain clear boundaries between projects, enabling teams to work autonomously on different parts of a system while also potentially reducing conflicts that might arise when making changes.

**Note:** This project contains the voting app monorepo.
copy vote, worker, result, and seed-data directory from [voting-app](https://github.com/dockersamples/example-voting-app) repository.

## Project Structure:
```bash
.
├── compose.yml
├── psql-test
│   └── psql-test.sh
├── ReadMe.md
├── result
│   ├── docker-compose.test.yml
│   ├── Dockerfile
│   ├── package.json
│   ├── package-lock.json
│   ├── server.js
│   ├── tests
│   │   ├── Dockerfile
│   │   ├── render.js
│   │   └── tests.sh
│   └── views
│       ├── angular.min.js
│       ├── app.js
│       ├── index.html
│       ├── socket.io.js
│       └── stylesheets
│           └── style.css
├── seed-data
│   ├── Dockerfile
│   └── make-data.py
├── vote
│   ├── app.py
│   ├── Dockerfile
│   ├── requirements.txt
│   ├── static
│   │   └── stylesheets
│   │       └── style.css
│   └── templates
│       └── index.html
└── worker
    ├── Dockerfile
    ├── Program.cs
    └── Worker.csproj
```

**Create `compose.yml` file and check service dependency and detail.**

## Gitlab cicd map and definitions:
The first step involves creating a design for the CI/CD process and pipeline structure.

![Pipeline Structure](images/pipeline-structure.png)

In the second step, we will complete the GitLab CI file step by step to create pipelines, stages, and jobs.

![voting pipeline steps](images/voting-pipeline-steps.png)

#
Global keywords that configure pipeline behavior:

- **default**	Custom default values for job keywords.
- **include**	Import configuration from other YAML files.
- **stages**	The names and order of the pipeline stages.
- **variables**	Define CI/CD variables for all job in the pipeline.
- **workflow**	Control what types of pipeline run.

#
**stages:** Use stages to define stages that contain groups of jobs. Use stage in a job to configure the job to run in a specific stage.

##### For example, we need six stages to build, test, and deploy the voting app.
```bash
stages:
  - release
  - build
  - test
  - deploy
  - post-deploy

```
  - **release**: For creating the voting app release.
  - **build**: For building all service images.
  - **test**:  For testing services after building and scanning containers.
  - **deploy**: For deploying the application to multiple environments.
  - **post-deploy**: For conducting load tests and other tasks after deploying the app.

#
**default:** You can set global defaults for some keywords. Each default keyword is copied to every job that doesn’t already have it defined. If the job already has a keyword defined, that default is not used.



